/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "lwip/opt.h"
#include "PWM.h"
#if LWIP_NETCONN & LWIP_IPV4 && LWIP_DHCP

#include "tcpecho/tcpecho.h"
#include "lwip/tcpip.h"
#include "lwip/udp.h"
#include "lwip/timeouts.h"
#include "lwip/init.h"
#include "lwip/dhcp.h"
#include "lwip/prot/dhcp.h"
#include "netif/ethernet.h"
#include "netif/etharp.h"
#include "ethernetif.h"

#include "board.h"

#include "fsl_device_registers.h"
#include "pin_mux.h"
#include "clock_config.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

#define EXAMPLE_ENET ENET
#define DHCP_TIMEOUT 10
#define configPHY_ADDRESS 1

/* IP address configuration. */
#define configIP_ADDR0 192
#define configIP_ADDR1 168
#define configIP_ADDR2 0
#define configIP_ADDR3 160

/* Netmask configuration. */
#define configNET_MASK0 255
#define configNET_MASK1 255
#define configNET_MASK2 255
#define configNET_MASK3 0

/* Gateway address configuration. */
#define configGW_ADDR0 192
#define configGW_ADDR1 168
#define configGW_ADDR2 0
#define configGW_ADDR3 1

/*! @brief Stack size of the temporary lwIP initialization thread. */
#define INIT_THREAD_STACKSIZE 1024

/*! @brief Priority of the temporary lwIP initialization thread. */
#define INIT_THREAD_PRIO DEFAULT_THREAD_PRIO

/*******************************************************************************
* Prototypes
******************************************************************************/

/*******************************************************************************
* Variables
******************************************************************************/

/*******************************************************************************
 * Code
 ******************************************************************************/
static void Init_DHCP_TCP(void* pvArguments) {
	 err_t err;
	 //static struct netif fsl_netif0;
	 struct dhcp *dhcp;
		static struct netif fsl_netif0;
    ip4_addr_t fsl_netif0_ipaddr, fsl_netif0_netmask, fsl_netif0_gw;

    LWIP_UNUSED_ARG(pvArguments);

    IP4_ADDR(&fsl_netif0_ipaddr, configIP_ADDR0, configIP_ADDR1, configIP_ADDR2, configIP_ADDR3);
    IP4_ADDR(&fsl_netif0_netmask, configNET_MASK0, configNET_MASK1, configNET_MASK2, configNET_MASK3);
    IP4_ADDR(&fsl_netif0_gw, configGW_ADDR0, configGW_ADDR1, configGW_ADDR2, configGW_ADDR3);

    tcpip_init(NULL, NULL);
	 

	 
	 /* Add a network interface to the list of lwIP netifs. */
	 netif_add(&fsl_netif0, &fsl_netif0_ipaddr, &fsl_netif0_netmask, &fsl_netif0_gw, NULL, ethernetif_init, tcpip_input);
	 /* Set the network interface as the default network interface. */
	 netif_set_default(&fsl_netif0);
	 // Make it active ...
	 netif_set_up(&fsl_netif0);
	 /* obtain the IP address, default gateway and subnet mask by using DHCP*/
	 err = dhcp_start(&fsl_netif0);
	 
	 PRINTF("%s : Started DCHP request (%s)\n", __FUNCTION__, lwip_strerr(err));
	  dhcp = (struct dhcp *)netif_get_client_data(&fsl_netif0, LWIP_NETIF_CLIENT_DATA_INDEX_DHCP);
	 for(int i=0; i < DHCP_TIMEOUT && dhcp->state != DHCP_STATE_BOUND; i++) {
	 PRINTF("%s : Current DHCP State : %d\n", __FUNCTION__, dhcp->state);
	 // Wait a second
	 vTaskDelay(1000/portTICK_PERIOD_MS);
	 }
	 
	
	 
	 PRINTF("%s : Interface is up : %d\n", __FUNCTION__, dhcp->state);
	 PRINTF("%s : IP %s\n", __FUNCTION__, ipaddr_ntoa(&fsl_netif0.ip_addr));
	 PRINTF("%s : NM %s\n", __FUNCTION__, ipaddr_ntoa(&fsl_netif0.netmask));
	 PRINTF("%s : GW %s\n", __FUNCTION__, ipaddr_ntoa(&fsl_netif0.gw));
	 
	 if (dhcp->state == DHCP_STATE_BOUND) {

    PRINTF("\r\n************************************************\r\n");
    PRINTF(" TCP Echo example\r\n");
    PRINTF("************************************************\r\n");

    tcpecho_init();

    vTaskDelete(NULL);
		}
	 // Wait a second
	 vTaskDelay(1000/portTICK_PERIOD_MS);
	 
	 /* finish the lease of the IP address */
	 err = dhcp_release(&fsl_netif0);
	 PRINTF("%s : DHCP Release (%s)\n", __FUNCTION__, lwip_strerr(err));
	 
	 for(;;) {};
}

static void Init_PWM(void* pvArguments){
	PRINTF("INITPWM\n");
	iniciaPWM();
	updatePWM(1, 100U);
	updatePWM(2, 100U);
	while(1){
		updatePWM(1, getPWM1());
		updatePWM(2, getPWM2());
		vTaskDelay(30);
	}

	
	
}

/*!
 * @brief Initializes lwIP stack.
 */
static void stack_init(void *arg)
{
    static struct netif fsl_netif0;
    ip4_addr_t fsl_netif0_ipaddr, fsl_netif0_netmask, fsl_netif0_gw;

    LWIP_UNUSED_ARG(arg);

    IP4_ADDR(&fsl_netif0_ipaddr, configIP_ADDR0, configIP_ADDR1, configIP_ADDR2, configIP_ADDR3);
    IP4_ADDR(&fsl_netif0_netmask, configNET_MASK0, configNET_MASK1, configNET_MASK2, configNET_MASK3);
    IP4_ADDR(&fsl_netif0_gw, configGW_ADDR0, configGW_ADDR1, configGW_ADDR2, configGW_ADDR3);

    tcpip_init(NULL, NULL);

    netif_add(&fsl_netif0, &fsl_netif0_ipaddr, &fsl_netif0_netmask, &fsl_netif0_gw, NULL, ethernetif_init, tcpip_input);
    netif_set_default(&fsl_netif0);
    netif_set_up(&fsl_netif0);

    PRINTF("\r\n************************************************\r\n");
    PRINTF(" TCP Echo example\r\n");
    PRINTF("************************************************\r\n");
    PRINTF(" IPv4 Address     : %u.%u.%u.%u\r\n", ((u8_t *)&fsl_netif0_ipaddr)[0], ((u8_t *)&fsl_netif0_ipaddr)[1],
           ((u8_t *)&fsl_netif0_ipaddr)[2], ((u8_t *)&fsl_netif0_ipaddr)[3]);
    PRINTF(" IPv4 Subnet mask : %u.%u.%u.%u\r\n", ((u8_t *)&fsl_netif0_netmask)[0], ((u8_t *)&fsl_netif0_netmask)[1],
           ((u8_t *)&fsl_netif0_netmask)[2], ((u8_t *)&fsl_netif0_netmask)[3]);
    PRINTF(" IPv4 Gateway     : %u.%u.%u.%u\r\n", ((u8_t *)&fsl_netif0_gw)[0], ((u8_t *)&fsl_netif0_gw)[1],
           ((u8_t *)&fsl_netif0_gw)[2], ((u8_t *)&fsl_netif0_gw)[3]);
    PRINTF("************************************************\r\n");

    tcpecho_init();

    vTaskDelete(NULL);
}
static void testtask(void *args)
{
	PRINTF("TST THREAD\n");
	while (1){
		GPIO_WritePinOutput(BOARD_M1_GPIO, BOARD_M1_PIN, 1U);
		GPIO_WritePinOutput(BOARD_M2_GPIO, BOARD_M2_PIN, 1U);
		vTaskDelay(300);
	}
		
}

static void tcptask(void *args)
{
	//tcpecho_init();
	PRINTF("TST THREAD2\n");
		while (1){
	}
		
}
/*!
 * @brief Main function
 */
int main(void)
{
		gpio_pin_config_t M1_config = {
        kGPIO_DigitalOutput, 0,
    };
		
		gpio_pin_config_t M2_config = {
        kGPIO_DigitalOutput, 0,
    };
		
    SYSMPU_Type *base = SYSMPU;
    BOARD_InitPins();
    BOARD_BootClockRUN();
		BOARD_InitDEBUG_UART();
    BOARD_InitDebugConsole();
		BOARD_InitENET();
    /* Disable SYSMPU. */
    base->CESR &= ~SYSMPU_CESR_VLD_MASK;
		GPIO_PinInit(BOARD_M1_GPIO, BOARD_M1_PIN, &M1_config);
		GPIO_PinInit(BOARD_M2_GPIO, BOARD_M2_PIN, &M2_config);
		
	
  if (xTaskCreate (
    		Init_DHCP_TCP,  // task function
			"DHCP_TCP", // task name for kernel awareness
			configMINIMAL_STACK_SIZE * 4, // task stack size
			(void*)NULL, // optional task startup argument
			tskIDLE_PRIORITY,  // initial priority
			(TaskHandle_t*)NULL // task handle
    ) != pdPASS)
    {
  	  for( ; ; ) { } // error! probably out of memory
    }

  // Creates the LwIP task
  if (xTaskCreate (
		Init_PWM,  // task function
		"PWM", // task name for kernel awareness
		configMINIMAL_STACK_SIZE * 4, // task stack size
		(void*)NULL, // optional task startup argument
		tskIDLE_PRIORITY,  // initial priority
		(TaskHandle_t*)NULL // task handle
	  ) != pdPASS)
  {
	  for( ; ; ) { } // error! probably out of memory
  }

  // Start FreeRTOS scheduler, does usually not return!
vTaskStartScheduler();
    /* Will not get here unless a task calls vTaskEndScheduler ()*/
    return 0;
	
	//V=2*PI/T
	//T=DeltaTempo/NCiclos
}
#endif
